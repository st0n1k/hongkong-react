import React from 'react';

import Contact from './Contact';
import { Switch, Route, Redirect, withRouter} from 'react-router-dom';

import Menu from './MenuComponent';
import Header from './Header';
import Footer from './Footer';
import Home from './Home';
import DishDetail from './DishDetail';
import About from './About';
import { postComment, fetchDishes, fetchComments, fetchPromos } from '../redux/actions/ActionCreators';
import { actions } from 'react-redux-form';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import { connect } from 'react-redux';

class Main extends React.Component {

  componentDidMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
  }

  render() {

    const HomePage = () => {
      return (
        <Home dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
        dishesLoading={this.props.dishes.isLoading} dishesErrMessage={this.props.dishes.errMess}
        promotion={this.props.promotions.promotions.filter((prom) => prom.featured)[0]}
        promosLoading={this.props.promotions.isLoading} promosErrMessage={this.props.promotions.errMess}
        leader={this.props.leaders.filter((leader) => leader.featured)[0]} />
      )
    }

    const DishWithId = ({match}) => {
      return (
        <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
        isLoading={this.props.dishes.isLoading} errMess={this.props.dishes.errMess} 
        comments={(this.props.comments.comments).filter(comm => comm.dishId === parseInt(match.params.dishId, 10))}
        commentsErrMess={this.props.comments.errMess}
        postComment={this.props.postComment} />
      )
    }

    return (
      <div>
        <Header />
        <TransitionGroup>
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
            <Switch>
              <Route path='/home' component={HomePage} />
              <Route exact path="/menu" component={() => <Menu dishes={this.props.dishes} />} />
              <Route path="/menu/:dishId" component={DishWithId} />
              <Route exact path="/contactus" component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm} />} />
              <Route exact path="/aboutus" component={() => <About leaders={this.props.leaders} />} />
              <Redirect to='/home' />
            </Switch>
          </CSSTransition>
        </TransitionGroup>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders
  }
};

const mapDispatchToProps = (dispatch) => ({
  postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
  fetchDishes: () => {dispatch(fetchDishes())},
  resetFeedbackForm: () => {dispatch(actions.reset('feedback'))},
  fetchComments: () => {dispatch(fetchComments())},
  fetchPromos: () => {dispatch(fetchPromos())}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
